# Fiche d'analyse risque

<!--
> le titre du risque est le titre de la issue
-->
- Libellé/Description du risque: 
- Nature du risque: 
- Commentaires:
- Date de création:
- Date de clôture:

## Facteurs du risque (causes)
<!--
> ne pas confondre le risque avec les facteurs de risques
-->

## Conséquences du risques (effets)
<!--
> on parle d'un risque, donc il n'est pas avéré, donc, ici on décrit ce qu'il pourrait arriver
-->

## Evaluation du risque

### Grille d'évaluation

- Probabilité d'apparition **(P)**

| Label      | Probabilité       |
| ---------- | ----------------- |
| Forte      | ~"PROBABILITY::3" |
| Moyenne    | ~"PROBABILITY::2" | 
| Faible     | ~"PROBABILITY::1" | 

- Gravité **(G)**

| Label      | Gravité           |
| ---------- | ----------------- |
| Forte      | ~"SEVERITY::3"    |
| Moyenne    | ~"SEVERITY::2"    | 
| Faible     | ~"SEVERITY::1"    | 

- Criticité **(C)** 

| Condition  | Criticité         |
| ---------- | ----------------- |
| P + G >= 5 | ~"CRITICALITY::3" |
| P + G = 4  | ~"CRITICALITY::2" | 
| P + G <= 2 | ~"CRITICALITY::1" | 

## Actions sur les facteurs du risque (causes)
> - permet de diminuer la criticité -> effet sur la probabilité d'apparition
> - utiliser les related issues

| Num  | Action (issue)    | Responsable(s)         | Fin prévue | Fin réalisée |
| ---- | ----------------- | ---------------------- | ---------- | ------------ |
|      |                   | @k33g                  |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |


## Actions sur les conséquences du risque (effets)
> - permet de diminuer la criticité -> effet sur les conséquences
> - utiliser les related issues

| Num  | Action (issue)    | Responsable(s)         | Fin prévue | Fin réalisée |
| ---- | ----------------- | ---------------------- | ---------- | ------------ |
|      |                   | @k33g                  |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
|      |                   |                        |            |              |
## Critères de clôtures du risque

> à quel moment on estime que le risque à disparu

/label ~RISK
/assign @k33g




